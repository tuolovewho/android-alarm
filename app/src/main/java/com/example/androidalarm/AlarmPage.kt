package com.example.androidalarm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class AlarmPage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm_page)

        val intent = getIntent()
        val ID = intent.getIntExtra("ID",0)

        Log.d("AlarmID2", ID.toString())
    }
}