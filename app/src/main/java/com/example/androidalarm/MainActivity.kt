package com.example.androidalarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import java.util.*

class MainActivity : AppCompatActivity() {

    var breakfastCheck: CheckBox? = null
    var lunchCheck: CheckBox? = null
    var dinnerCheck: CheckBox? = null
    var beforeBedtimeCheck: CheckBox? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun update(view: View){

        Log.d("AlarmID", "Click")

        breakfastCheck = findViewById(R.id.breakfastCheck);
        lunchCheck = findViewById(R.id.lunchCheck);
        dinnerCheck = findViewById(R.id.dinnerCheck);
        beforeBedtimeCheck = findViewById(R.id.beforeBedtimeCheck);

        var intent = Intent(this@MainActivity, AlarmReceiver::class.java)

        var calendar = Calendar.getInstance();

        var time1 = calendar.timeInMillis + 5*1000
        var time2 = calendar.timeInMillis + 10*1000

        val alarmManager = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        var ID = 4

        if (breakfastCheck?.isChecked() == true) {
            intent.putExtra("ID", ID)

            var pi = PendingIntent.getBroadcast(this, ID, intent, 0)

            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                time1,
                1000 * 60 * 60 * 24,
                pi
            )
        } else {
            var pi =
                PendingIntent.getBroadcast(this, ID, intent, PendingIntent.FLAG_NO_CREATE)

            var alarmUp = (PendingIntent.getBroadcast(
                this, ID,
                intent,
                PendingIntent.FLAG_NO_CREATE
            ) != null)

            if (alarmUp) {
                alarmManager.cancel(pi)
            }
        }

        ID = 5

        if (lunchCheck?.isChecked() == true) {
            intent.putExtra("ID", ID)

            var pi = PendingIntent.getBroadcast(this, ID, intent, 0)

            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                time2,
                1000 * 60 * 60 * 24,
                pi
            )

        } else {
            var pi =
                PendingIntent.getBroadcast(this, ID, intent, PendingIntent.FLAG_NO_CREATE)

            var alarmUp = (PendingIntent.getBroadcast(
                this, ID,
                intent,
                PendingIntent.FLAG_NO_CREATE
            ) != null)

            if (alarmUp) {
                alarmManager.cancel(pi)
            }
        }

    }
}