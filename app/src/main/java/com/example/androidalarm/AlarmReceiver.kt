package com.example.androidalarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val ID = intent.getIntExtra("ID", 0)

        Log.d("AlarmID1", ID.toString())

        var intentContent = Intent(context, AlarmPage::class.java);
        intentContent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentContent.putExtra("ID", ID)
        context!!.startActivity(intentContent);
    }
}